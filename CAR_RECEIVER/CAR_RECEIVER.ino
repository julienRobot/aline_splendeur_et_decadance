#include "car.h"

// constants won't change. Used here to set a pin number:
const int ledPin =  LED_BUILTIN;// the number of the LED pin
int ledState = LOW;             // ledState used to set the LED
long ledtick=0;

#define SERIAL_PRINT_CTRL_VALUES 1



#define PRINTRAWDATA

#define COMPUTERSerial Serial
#define XBEESerial Serial1

Car car1(1);
Car car2(2);
Car car3(3);
Car car4(4);

Car lidar(5);

char startup = 1;
char assignMode = 0;

void setup() {

  pinMode(ledPin, OUTPUT);                // set the led pin as output:
  pinMode(12, OUTPUT);                    // set test pin 12 as output
  pinMode(10, OUTPUT);                    // set test pin 10 as output

  pinMode(6, OUTPUT);                    // set test pin 10 as output
  digitalWrite(15, LOW);
  pinMode(8, INPUT_PULLUP);                    // set test pin 10 as output

  Serial.println("race started..."); 

  Car *device;

  struct frame_s *newFrame;

  while(1){

      if( isCarFrameAvailable() ) {

          digitalWrite(12, HIGH);

          newFrame = getNewCarFrame();

          switch( newFrame->SrcAdd ){
              case 0x101 :              //Serial.print("CAR1\t");Serial.println(newFrame->SrcAdd,HEX);
                                        device = &car1;          
              break;

              case 0x102 :              //Serial.print("CAR2\t"); Serial.println(newFrame->SrcAdd,HEX);
                                        device = &car2;
              break;

              case 0x103 :              //Serial.print("CAR3\t"); Serial.println(newFrame->SrcAdd,HEX);
                                        device = &car3;
              break;

              case 0x104 :              //Serial.print("CAR4\t"); Serial.println(newFrame->SrcAdd,HEX);
                                        device = &car4;
              break;

              case 0x105 :              //Serial.print("SCAN\t"); Serial.println(newFrame->SrcAdd,HEX);
                                        device = &lidar;
              break;
          }

          // udpate device variables
          char speed = device->mapSpeed( newFrame->data[0]<<8 | newFrame->data[1]);
          char wheel = device->mapWheel( newFrame->data[2]<<8 | newFrame->data[3]);

          if (SERIAL_PRINT_CTRL_VALUES == 1){
            Serial.print("CAR");
            Serial.print(newFrame->SrcAdd,HEX);
            Serial.print("\ts: "); Serial.print(speed,DEC);
            Serial.print("\tw: "); Serial.print(wheel,DEC);
            Serial.print("\t\t rssi: ");Serial.println(newFrame->rssi, DEC);
          }



          device->processNewData(); // do whatever need to be done with the new data, send midi etc...

          digitalWrite(12, LOW);
      }

      /* 
       *  check for policer to push the fucking button
       *  
       */
      long btnTick;
      static char btnInibit = 0;
      
      if( digitalRead(8) == LOW && btnInibit == 0) {
        
        btnTick = millis();
        
        while( (digitalRead(8) == LOW) && ((millis()-btnTick)<3000)  ){
        }

        if( digitalRead(8) == LOW ) {
            btnInibit = 1;
            CarToggleFilter();
        }
        
      }

      if( digitalRead(8) == HIGH){
            btnInibit = 0;
      }  

  }



  
  
  delay(1000);  

}

int rx_cnt = 0;
int rx_buff[2000] ;
int rx_pntr = 0;

long ms=0;
 // boulce principal du programme

uint8_t reverse = 0;
uint8_t forward = 0;
uint8_t left = 0;
uint8_t right = 0;
uint8_t dir = 0;

uint8_t reverse_last = 0;
uint8_t forward_last = 0;
uint8_t left_last = 0;
uint8_t right_last = 0;

char moving = 0;
char arriere = 0;
char stoping = 0;
char fw_max = 0;
char bw_max = 0;
char left_max = 0;
char right_max = 0;
char left_cnt = 0;
char right_cnt = 0; 

int radioSyncCounts = 0 ;
char radioSyncData = 0;
 
void loop() {

   // Loop back data from USB Serial to Xbee Serial
   while (Serial.available()){ 
     char dat = (char)Serial.read();
     Serial1.write(dat);
   }

  sendSyncFrame_polling(); // send sync frame every 50 ms
  
  // ZIGBEE Serial port read
  while (Serial1.available()) { // check if data from ZIG

    uint8_t inChar = (char)Serial1.read(); // read Zigbee serial port char
    
    //Serial.write(inChar); // loop back char to computer Serial USB
    
    ledState = HIGH;
    digitalWrite(ledPin, ledState);
    ledtick = millis();
    
    /*char res;
    res = buildFrame(inChar);

    if( res == 0 ){
      //if( frame.data[02] == 0x03){
        
          int speed = frame.data[5]<<8 | frame.data[6];
          int wheel = frame.data[7]<<8 | frame.data[8];

          //Serial.print("src: \t");
          //Serial.print(frame.data[2],  DEC);
          //Serial.print(",\t\t");
          
         // if( frame.data[2] == 4 ){
            //Serial.print(speed,  DEC);
          //Serial.print(",\t\t");
          //Serial.print(speed,  DEC);

          //Serial.print(",\t\t");
          Serial.println(speed,  DEC);
         // }
    }*/
          
          /*
          if( speed <= 1500 ){
              forward = map(speed, 1500, 900, 63, 0);
          }else if( speed >= 1515 ){
              forward = map(speed, 1515, 2115, 63, 126);
          }else{
              forward = 63;
          }

          if( wheel <= 1480 ){
              dir = map(wheel, 1480, 900, 63, 0);
          }else if( wheel >= 1505 ){
              dir = map(wheel, 1505, 2083, 63, 126);
          }else{
              dir = 63;
          }

         static long startupTick=0;

         if( assignMode == 0){
          
           if( forward_last != forward){
              usbMIDI.sendControlChange( 75,        forward,     1);
              forward_last = forward;
           }
  
           if( left_last != dir){
              usbMIDI.sendControlChange( 77,        dir,        1); 
              left_last = dir;
           } 
         }

          if( moving == 0){
            if(forward > 75 || forward < 58 ){
              moving = 1;
              usbMIDI.sendNoteOn(24, 100, 1);
              //Serial.println("note 24");
            }
         }else{
            if(forward == 63 ){
              moving = 0;
              usbMIDI.sendNoteOn(25, 100, 1);
              //Serial.println("note 25");
            }
         } 

         static char arriere_cnt=0;

         if( arriere == 0){
            if(forward < 20){
              arriere = 1;

              arriere_cnt++;
              if(arriere_cnt >=3){
                arriere_cnt = 0;  
              }
              
              usbMIDI.sendNoteOn(26+arriere_cnt, 100, 1);

            }
         }else{
            if(forward == 63 ){
              arriere = 0;
            }
         } 


         if( left_max == 0){
            if(dir > 120){
              left_max = 1;

              if( left_cnt++ >= 5){
                left_cnt = 0;
              }
              Serial.println("l");
              usbMIDI.sendNoteOn(36+left_cnt, 100, 1);

            }
         }else{
            if(dir == 63){
              left_max = 0;
              Serial.println("ls");
              usbMIDI.sendNoteOn(36+left_cnt, 0, 1);
            }
         }  
         
         if( right_max == 0){
            if(dir < 50){
              right_max = 1;

              if( right_cnt++ >= 5){
                right_cnt = 0;
              }
              Serial.println("r");
              usbMIDI.sendNoteOn(48+right_cnt, 100, 1);
              
            }
         }else{
            if(dir == 63){
              right_max = 0;
              Serial.println("rs");
              usbMIDI.sendNoteOn(48+right_cnt, 0, 1);
            }
         }  
         
                 
      }*/
      
   /* }*/
  
  digitalWrite(10, LOW);
  }


  // if LED is on we turn it of after 0.1 second
  if( ledState == HIGH ){ 
    if( (millis() - ledtick) > 100){
      ledState = LOW;
      digitalWrite(ledPin, ledState);
    }
  }
  
} 






void sendSyncFrame_polling(void){
  
     if( (millis() - ms) > 50){

      radioSyncCounts++;
      if( radioSyncCounts >= 20){
        radioSyncCounts = 0;
        radioSyncData = 0x89;
      }else{
        radioSyncData = 0x88;  
      }

      digitalWrite(12, HIGH);
    
      ms = millis();
      // From XCTU Frame generator 7E 00 06 01 01 FF FF 01 88 76
      Serial1.write(0x7E);  // start delimiter
      Serial1.write(0x00);  // lenght
      Serial1.write(0x06);  // lenght
      Serial1.write(0x01);  // frame type  ??
      Serial1.write(0x01);  // frame ID ???
      Serial1.write(0xFF);  // Add ff broadcast
      Serial1.write(0xFF);  // add ff broadcast
      Serial1.write(0x01);  // option 
      Serial1.write(0x88);  // data 
      Serial1.write(0x76);  // CRC

      digitalWrite(12, LOW);
   }
   
}





  
 

  

      


    
  

  
  


// API FRAME FORMAT
// Example :

// Start     Lenght       Type  source add                src nwrk add  Option                            Checksum
// 7E        00   14      90    00 13 A2 00 41 7B 6A BB   83 CE         01      62 6F 6E 6A 6F 75 72 0D   7B 
//           MSB  LSB     Rx Packet                                     ack     b  o  n  j  o  u  r  \r 

// source: https://www.digi.com/resources/documentation/Digidocs/90002002/Reference/r_frame_0x90.htm?TocPath=API%20Operation%7CFrame%20descriptions%7C_____10




// Tableau d'assignement des messages recu des voitures
 
// -----------------------------------------------------
// PARAMETRE                                    MSG TYPE
// -----------------------------------------------------
// volant, course gauche :                -->   CC    75
// volant, course droite :                -->   CC    75
// volant, fin de course gauche :         -->   CC    75
// volant, fin de course droite :         -->   CC    75

// gachette, sortie de l'etat de repos    -->   CC    75
// gachette, entree dans l'etat de repos  -->   CC    75
// gachette, course marche avant          -->   CC    75
// gachette, course marche arriere        -->   CC    75

  /*Serial.println("> Send +++"); // consol
  Serial1.print("+++"); // To Xbee
  switch( Xbee_isOk() ){
    case 0:   Serial.println("Entered AT Mode"); // consol
    break;
    case 1:   Serial.println("timeout..."); // consol
    break;
    case 2:   Serial.println("unknown resp..."); // consol
    break;
  }
  Serial.println("> send AT"); // consol
  Serial1.print("AT\r"); // To Xbee
  switch( Xbee_isOk() ){
    case 0:   Serial.println("Xbee detected"); // consol
    break;
    case 1:   Serial.println("timeout..."); // consol
    break;
    case 2:   Serial.println("unknown resp..."); // consol
    break;
 }
  while(1);*/

uint8_t Xbee_isOk(void){
    char Chars[2];
    char Add=0;
    long timeoutTick=0;
    timeoutTick = millis();
    while ( 1 ){ //(Add < 2)){
      if( Serial1.available() ) {
        Chars[Add]=(char)Serial1.read();
        Serial.print(Chars[Add]);
        //Add++;
      }
      if(millis()-timeoutTick > 1000 ){
          return 1;
      }
    }
    if( Chars[0] == 'O' && Chars[1] == 'K'){
      return 0;
    }else{
      return 2;
    }
    Serial.println("<<raw");
}

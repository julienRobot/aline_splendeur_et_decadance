#include "arduino.h"
#include "car.h"

char Initalized = 0;

IntervalTimer syncTimer;

char ctrlChFilterOn       = 0;

void sendSyncFrame(void);
char buildFrame(uint8_t nuChar);

int  ledPin   = LED_BUILTIN;// the number of the LED pin
long ledTick  = 0;
char ledTempo = 0;

void CarToggleFilter(void){
  if( ctrlChFilterOn == 1 ) {
      ctrlChFilterOn = 0;
  }else{
      ctrlChFilterOn = 1;  
  }
}


Car::Car(char midiChannel){

  if ( Initalized == 0 ){ // initialisation of Xbee module only at first car instantiation 

    delay(1000);
    
    Initalized = 1;
    Serial1.begin(115200);                  // Xbee serial at 115200
    Serial.println("Init engines..."); 

    syncTimer.begin(sendSyncFrame, 50000);  // blinkLED to run every 0.15 seconds
  }

  this->midiChannel = midiChannel;
  this->carNumber   = midiChannel;
}

char Car::mapSpeed(int rawData){

    int newValue;

    rawData = constrain(rawData, RAWSPEED_MIN, RAWSPEED_MAX);
  
    if( rawData >= RAWSPEED_ZERO){
        newValue = map(rawData, RAWSPEED_ZERO, RAWSPEED_MAX,   63,  127);
    }else{
        newValue = map(rawData, RAWSPEED_ZERO, RAWSPEED_MIN,   63,  0);    
    }

    if( this->speed != newValue){
      this->speed = newValue;
      this->speedFlagNew = 1;
    }

    return this->speed;
}

char Car::mapWheel(int rawData){

    int newValue;

    rawData = constrain(rawData, RAWDIR_MIN, RAWDIR_MAX);
  
    if( rawData >= RAWDIR_ZERO){
        newValue = map(rawData, RAWDIR_ZERO, RAWDIR_MAX,   63,  127);
    }else{
        newValue = map(rawData, RAWDIR_ZERO, RAWDIR_MIN,   63,  0);    
    }

    if( this->wheel != newValue){
      this->wheel = newValue;
      this->wheelFlagNew = 1;
    }

    return this->wheel; 
}

void Car::processNewData(void){

    
          if ( this->speedFlagNew == 1  ){
              this->speedFlagNew = 0;

              /*
               * SEND SPEED CONTROL CHANGES 
               */
      
              if( ctrlChFilterOn == 0 )   {
                  //usbMIDI.sendControlChange(this->midiCtrlChange_speed, this->speed, this->midiChannel);
                  //Serial.println(this->speed, DEC);
              }
      
               /*
               * SEND SPEED NOTES 
               */
      
              if( this->speed < 43 ){//53
      
                  if( this->start == 0){
                      this->start = 1;

                      usbMIDI.sendNoteOn(120, 127 , this->midiChannel);//**
                      usbMIDI.sendNoteOn(121, 127 , this->midiChannel);//**
                      
                      //Serial.print(this->carNumber, DEC);
                      //Serial.println(" MoveRev"); //Serial.println(this->midiNote_speed_move + this->midiNote_speed_move_index, DEC);
                  }
      
                  if( this->speed < 10 ){
                      if( this->minSpeed == 0){
                          this->minSpeed = 1;
                          
                          usbMIDI.sendNoteOn(48 + this->midiNote_speed_fullRev_index, 127 , this->midiChannel);//**
      
                          this->midiNote_speed_fullRev_index++;
                          if( this->midiNote_speed_fullRev_index >= 24 ){
                            this->midiNote_speed_fullRev_index = 0;
                          }
                          
                          //Serial.print(this->carNumber, DEC);
                          //Serial.println(" minSpeed"); //Serial.println(this->midiNote_speed_fullRev + this->midiNote_speed_fullRev_index, DEC);
                      }
                  }
      
              }else if( this->speed < 83 ){//73
      
                  if( this->start == 1){
                      this->start = 0;
                      
                      usbMIDI.sendNoteOn(122, 127 , this->midiChannel);//**
                      usbMIDI.sendNoteOn(123, 127 , this->midiChannel);//**

                      usbMIDI.sendNoteOn(24 + this->midiNote_speed_stop , 127 , this->midiChannel);//**
                      
                      this->midiNote_speed_stop++;
                      if( this->midiNote_speed_stop >= 24 ){
                        this->midiNote_speed_stop = 0;
                      }

                      
                      //Serial.print(this->carNumber, DEC);
                      //Serial.println(" Stop"); //Serial.println(this->midiNote_speed_stop + this->midiNote_speed_stop_index, DEC);
                  }
      
                  this->maxSpeed = 0;
                  this->minSpeed = 0;
              
              }else{
      
                  if( this->start == 0){
                      this->start = 1;
    
                      //Serial.print(this->carNumber, DEC);
                      //Serial.println(" MoveFw");// Serial.println(this->midiNote_speed_move + this->midiNote_speed_move_index, DEC);

                      this->midiNote_speed_move_index++;
                      if( this->midiNote_speed_move_index >= 24 ){
                        this->midiNote_speed_move_index = 0;
                      }

                      usbMIDI.sendNoteOn(0 + this->midiNote_speed_stop, 127 , this->midiChannel);//**

                      usbMIDI.sendNoteOn(120, 127 , this->midiChannel);//**
                      usbMIDI.sendNoteOn(121, 127 , this->midiChannel);//**

                  }
      
                  if( this->maxSpeed == 0 && this->speed > 117){
                      this->maxSpeed = 1;

                     //int randomNote = random(1, 100);
                      //if( randomNote < 21 ){
                      //    usbMIDI.sendNoteOn(72, 127 , this->midiChannel);
                      //}

                      /*usbMIDI.sendNoteOn(48 + this->midiNote_speed_full_index , 127 , this->midiChannel); //**

                      this->midiNote_speed_full_index++;
                      if( this->midiNote_speed_full_index >= 24 ){
                        this->midiNote_speed_full_index = 0;
                      }*/
                      
                      //Serial.print(this->carNumber, DEC);
                      //Serial.println(" maxSpeed note  "); //Serial.println(this->midiNote_dir_full + this->midiNote_dir_full_index, DEC);
                  }
              }
          }

          if ( this->wheelFlagNew == 1  ){
            
              this->wheelFlagNew = 0;

              if( ctrlChFilterOn == 0 )   {
                  usbMIDI.sendControlChange(this->midiCtrlChange_wheel, this->wheel, this->midiChannel);
              }
          
              if( this->wheel < 10 ){

                  if( this->maxLeft == 0 ){
                    this->maxLeft = 1;
                    
                    //Serial.print(this->carNumber, DEC);
                    //Serial.println(" maxleft"); //Serial.println(this->midiNote_dir_full + this->midiNote_dir_full_index, DEC);

                    usbMIDI.sendNoteOn(72 + this->midiNote_dir_fullRev_index , 127 , this->midiChannel);
          
                    this->midiNote_dir_fullRev_index++;
                    if( this->midiNote_dir_fullRev_index >= 24 ){
                      this->midiNote_dir_fullRev_index = 0;
                    }
                    
                  }
      
              }else if( this->wheel > 53 && this->wheel < 73){

                    if( this->maxLeft == 1){
                      
                      this->maxLeft = 0;
                      
                      //Serial.print(this->carNumber, DEC);
                      //Serial.println(" center from left"); //Serial.println(this->midiNote_dir_full + this->midiNote_dir_full_index, DEC);

                      //usbMIDI.sendNoteOn(48 + this->midiNote_dir_stop_index , 127 , this->midiChannel);
          
                      this->midiNote_dir_stop_index++;
                      if( this->midiNote_dir_stop_index >= 6 ){
                        this->midiNote_dir_stop_index = 0;
                      }
                    
                    }
                    
                    if( this->maRight == 1 ){
                      this->maRight = 0;
                      
                      //Serial.print(this->carNumber, DEC);
                      //Serial.println(" center from right"); //Serial.println(this->midiNote_dir_full + this->midiNote_dir_full_index, DEC);
                      
                      //usbMIDI.sendNoteOn(48 + this->midiNote_dir_stop_index , 127 , this->midiChannel);
          
                      this->midiNote_dir_stop_index++;
                      if( this->midiNote_dir_stop_index >= 6 ){
                        this->midiNote_dir_stop_index = 0;
                      }
                    
                    }
                    

              }else if( this->wheel > 117){

                  if( this->maRight == 0 ) {
                    this->maRight = 1;

                    usbMIDI.sendNoteOn(96 + this->midiNote_dir_full_index , 127 , this->midiChannel);
          
                    this->midiNote_dir_full_index++;
                    if( this->midiNote_dir_full_index >= 24 ){
                      this->midiNote_dir_full_index = 0;
                    }

                    //Serial.print(this->carNumber, DEC);
                    //Serial.println(" maxRight"); //Serial.println(this->midiNote_dir_full + this->midiNote_dir_full_index, DEC);
                  }    
              }

         }

}

struct frame_s frame;

char isCarFrameAvailable(void){ // if return something != 0, data have been received

  while ( Serial1.available() ) { // check if data from ZIG

      uint8_t inChar = (char)Serial1.read(); // read Zigbee serial port char

      if( buildFrame(inChar) == 0 ){ // return 1 when a new frame from a car is fully received
          digitalWrite(10, HIGH); // test pin to scope we are receiving
          return 1; // return network device number
      }
  }

  digitalWrite(10, LOW); // test pin to scope we are receiving
  return 0;
}

struct frame_s* getNewCarFrame(void){ // if return something != 0, data have been received
  return &frame;
}



char buildFrame(uint8_t nuChar){

  static int pointer = 0;
  static int charCounter = 0;

  switch( pointer ){
    
      case 0: // header
        if(nuChar == 0x7E){
          pointer++;
          charCounter = 0;
          frame.lenght = 0;
          frame.SrcAdd = 0;
          frame.header = 0x7E;
          return 1;
        }else{
          pointer = 0;
          return 1;
        }
      break;
  
      case 1: // size MSB 
        frame.lenght = frame.lenght | nuChar << 8;
        pointer++;
        return 1;
      break;
  
      case 2: // size LSB
        frame.lenght = frame.lenght |  nuChar;
        pointer++;
        
        return 1;
      break;

      case 3: // size MSB 
        frame.API = nuChar;

        if( frame.API== 0x81){ // only accept message from devices
          pointer++;
        }else{
          pointer = 0;
        }
          
        return 1;
      break;

      case 4: // size MSB 
        frame.SrcAdd = frame.SrcAdd | nuChar << 8;
        pointer++;
        return 1;
      break;
  
      case 5: // size LSB
        frame.SrcAdd = frame.SrcAdd |  nuChar;
        pointer++;
        return 1;
      break;

      case 6: // size LSB
        frame.rssi = nuChar;
        pointer++;
        return 1;
      break;

     case 7: // size LSB
        frame.option = nuChar;
        pointer++;
        return 1;
      break;

      default:
        if( charCounter < (frame.lenght - 5) ){
           frame.data[charCounter] = nuChar;
           charCounter++;
           return 1;
        }else{
           frame.checksum = nuChar;
           pointer = 0;
           return 0;
        }
      break;
  }
  
}


void sendSyncFrame(void){
  /* Blink led when sending sync frame */
  char syncMsg;
  
  ledTempo++;
  if( ledTempo >= 19 ){
      ledTempo = 0;  
      syncMsg = 0x89;
  }else{
    syncMsg = 0x88;
  }
  switch( ledTempo ){
    case 0 :  
      digitalWrite(ledPin, HIGH); 
    break;
  
    case 1 :
      digitalWrite(ledPin, LOW); 
    break;

    case 2 :  

      if( ctrlChFilterOn == 0 ){
        digitalWrite(ledPin, HIGH);
      } 
    break;
  
    case 3 :
      digitalWrite(ledPin, LOW); 
    break;
  } 


  Serial1.write(0x7E);  // start delimiter
  Serial1.write(0x00);  // lenght
  Serial1.write(0x06);  // lenght
  Serial1.write(0x01);  // frame type  ??
  Serial1.write(0x01);  // frame ID ???
  Serial1.write(0xFF);  // Add ff broadcast
  Serial1.write(0xFF);  // add ff broadcast
  Serial1.write(0x01);  // option 
  Serial1.write(0x88);  // data 
  Serial1.write(0x76);  // CRC
  
}





/*struct car_s CAR1 = {
    .midiChannel                  = 1,    // CAR's Midi channel
    
    .midiNote_dir_move            = 12,   // C0 ( note start )
    .midiNote_dir_move_range      = 4,    //    ( note variations )
    
    .midiNote_dir_stop            = 24,   // C1 ( note start )
    .midiNote_dir_range           = 4,    //    ( note variations )
    
    .midiNote_dir_full            = 36,   // C2 ( note start )
    .midiNote_dir_full_range      = 4,    //    ( note variations )
    
    .midiNote_dir_fullRev         = 48,   // C3 ( note start )
    .midiNote_dir_fullRev_range   = 4,    //    ( note variations )

    .midiNote_speed_move          = 60,   // C4 ( note start )
    .midiNote_speed_move_range    = 4,    //    ( note variations )
    
    .midiNote_speed_stop          = 72,    // C5 ( note start )
    .midiNote_speed_range         = 4,    //    ( note variations )
    
    .midiNote_speed_full          = 84,   // C6 ( note start )
    .midiNote_speed_full_range    = 4,    //    ( note variations )
    
    .midiNote_speed_fullRev       = 96,   // C7 ( note start )
    .midiNote_speed_fullRev_range = 4,    //    ( note variations )

    .midiCtrlNum_speed            = 20,   // Midi ctrl change number for speed
    .midiCtrlNum_dir              = 21,   // Midi ctrl change number for Direction
};

struct car_s CAR2 = {
    .midiChannel                  = 2,    // CAR's Midi channel
    
    .midiNote_dir_move            = 12,   // C0 ( note start )
    .midiNote_dir_move_range      = 4,    //    ( note variations )
    
    .midiNote_dir_stop            = 24,   // C1 ( note start )
    .midiNote_dir_range           = 4,    //    ( note variations )
    
    .midiNote_dir_full            = 36,   // C2 ( note start )
    .midiNote_dir_full_range      = 4,    //    ( note variations )
    
    .midiNote_dir_fullRev         = 48,   // C3 ( note start )
    .midiNote_dir_fullRev_range   = 4,    //    ( note variations )

    .midiNote_speed_move          = 60,   // C4 ( note start )
    .midiNote_speed_move_range    = 4,    //    ( note variations )
    
    .midiNote_speed_stop          = 72,    // C5 ( note start )
    .midiNote_speed_range         = 4,    //    ( note variations )
    
    .midiNote_speed_full          = 84,   // C6 ( note start )
    .midiNote_speed_full_range    = 4,    //    ( note variations )
    
    .midiNote_speed_fullRev       = 96,   // C7 ( note start )
    .midiNote_speed_fullRev_range = 4,    //    ( note variations )

    .midiCtrlNum_speed            = 20,   // Midi ctrl change number for speed
    .midiCtrlNum_dir              = 21,   // Midi ctrl change number for Direction
};

struct car_s CAR3 = {
    .midiChannel                  = 3,    // CAR's Midi channel
    
    .midiNote_dir_move            = 12,   // C0 ( note start )
    .midiNote_dir_move_range      = 4,    //    ( note variations )
    
    .midiNote_dir_stop            = 24,   // C1 ( note start )
    .midiNote_dir_range           = 4,    //    ( note variations )
    
    .midiNote_dir_full            = 36,   // C2 ( note start )
    .midiNote_dir_full_range      = 4,    //    ( note variations )
    
    .midiNote_dir_fullRev         = 48,   // C3 ( note start )
    .midiNote_dir_fullRev_range   = 4,    //    ( note variations )

    .midiNote_speed_move          = 60,   // C4 ( note start )
    .midiNote_speed_move_range    = 4,    //    ( note variations )
    
    .midiNote_speed_stop          = 72,    // C5 ( note start )
    .midiNote_speed_range         = 4,    //    ( note variations )
    
    .midiNote_speed_full          = 84,   // C6 ( note start )
    .midiNote_speed_full_range    = 4,    //    ( note variations )
    
    .midiNote_speed_fullRev       = 96,   // C7 ( note start )
    .midiNote_speed_fullRev_range = 4,    //    ( note variations )

    .midiCtrlNum_speed            = 20,   // Midi ctrl change number for speed
    .midiCtrlNum_dir              = 21,   // Midi ctrl change number for Direction
};

struct car_s CAR4 = {
    .midiChannel                  = 4,    // CAR's Midi channel
    
    .midiNote_dir_move            = 12,   // C0 ( note start )
    .midiNote_dir_move_range      = 4,    //    ( note variations )
    
    .midiNote_dir_stop            = 24,   // C1 ( note start )
    .midiNote_dir_range           = 4,    //    ( note variations )
    
    .midiNote_dir_full            = 36,   // C2 ( note start )
    .midiNote_dir_full_range      = 4,    //    ( note variations )
    
    .midiNote_dir_fullRev         = 48,   // C3 ( note start )
    .midiNote_dir_fullRev_range   = 4,    //    ( note variations )

    .midiNote_speed_move          = 60,   // C4 ( note start )
    .midiNote_speed_move_range    = 4,    //    ( note variations )
    
    .midiNote_speed_stop          = 72,    // C5 ( note start )
    .midiNote_speed_range         = 4,    //    ( note variations )
    
    .midiNote_speed_full          = 84,   // C6 ( note start )
    .midiNote_speed_full_range    = 4,    //    ( note variations )
    
    .midiNote_speed_fullRev       = 96,   // C7 ( note start )
    .midiNote_speed_fullRev_range = 4,    //    ( note variations )

    .midiCtrlNum_speed            = 20,   // Midi ctrl change number for speed
    .midiCtrlNum_dir              = 21,   // Midi ctrl change number for Direction
};


void cars_initXbee(void){

  Serial1.begin(115200);                  // Xbee serial at 115200
  
}*/
















               // PRINT FRAMES 
               /*Serial.print("> Head:\t");           Serial.print(  frame.header, HEX);
               Serial.print("\tsize:\t");           Serial.print(  frame.lenght, DEC);
               Serial.print("\tadd:\t");            Serial.print(  frame.SrcAdd, HEX);    
               Serial.print("\trssi:\t");           Serial.print(  frame.rssi,   DEC);  
               Serial.print("\tOption:\t");         Serial.print(  frame.option, HEX); 
               Serial.print("\tDAT:\t");
               for (int i=0 ; i<frame.lenght-5 ; i++){
                  Serial.print(  frame.data[i], HEX);Serial.print(","); 
               }
               Serial.print("\tcrc:\t");         Serial.println(  frame.checksum, HEX); 
               */




// TESTGETMOVE STATE



  /*switch ( car1.getMoveState() ){
    
    case carSTOPED: Serial.println("carSTOPED");
    break;
    
    case carFORWARD: Serial.println("carFORWARD");
    break;

    case carBACKWARD: Serial.println("carBACKWARD");
    break;
  } 
  
  switch ( car1.getDirState() ){
    
    case carSTRAIGHT: Serial.println("carSTRAIGHT");
    break;
    
    case carLEFT: Serial.println("carLEFT");
    break;

    case carRIGHT: Serial.println("carRIGHT");
    break;
  } */

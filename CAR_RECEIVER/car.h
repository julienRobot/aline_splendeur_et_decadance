#ifndef CAR_H_
#define CAR_H_

#include "arduino.h"

#define RAWSPEED_ZERO   1500
#define RAWSPEED_MAX    2050
#define RAWSPEED_MIN     950

#define RAWDIR_ZERO     1500
#define RAWDIR_MAX      2050
#define RAWDIR_MIN       950

enum carMoveState_e{
  carSTOPED     ,
  carFORWARD    ,
  carBACKWARD
};

enum carDirState_e{
  carSTRAIGHT    ,
  carLEFT        ,
  carRIGHT         
};

struct frame_s{
  char header;
  int  lenght;
  char API;
  long SrcAdd;
  char rssi;
  char option;
  char data[256];
  char checksum;
};

class Car{

  public:

    Car(char midiChannel);
    void test(void);
    char isCarSpeaking(void);
    void processNewData(void);
    
    char mapSpeed(int rawData);
    char mapWheel(int rawData);
    
    char carNumber                    = 1;    // CAR's Midi channel
    
  private :

    float speedAttenuation            = 0.1;

    // MIDI PARAMETERS :


    char midiChannel                  = 1;    // CAR's Midi channel
    
    char midiNote_dir_move            = 12;   //    C0 ( note start )
    char midiNote_dir_move_index      = 0;    //    ( note variations )
    char midiNote_dir_move_range      = 4;    //    ( note variations )
    
    char midiNote_dir_stop            = 24;   // C1 ( note start )
    char midiNote_dir_stop_index      = 0;   // C1 ( note start )
    char midiNote_dir_range           = 4;    //    ( note variations )
    
    char midiNote_dir_full            = 36;   // C2 ( note start )
    char midiNote_dir_full_index      = 0;   // C2 ( note start )
    char midiNote_dir_full_range      = 4;    //    ( note variations )
    
    char midiNote_dir_fullRev         = 48;   // C3 ( note start )
    char midiNote_dir_fullRev_index   = 0;   // C3 ( note start )
    char midiNote_dir_fullRev_range   = 4;    //    ( note variations )

    char midiNote_speed_move          = 60;   // C4 ( note start )
    char midiNote_speed_move_index    = 0;   // C4 ( note start )
    char midiNote_speed_move_range    = 4;    //    ( note variations )
    
    char midiNote_speed_stop          = 72;    // C5 ( note start )
    char midiNote_speed_stop_index    = 0;    // C5 ( note start )
    char midiNote_speed_stop_range    = 4;    //    ( note variations )
    
    char midiNote_speed_full          = 84;   // C6 ( note start )
    char midiNote_speed_full_index    = 0;   // C6 ( note start )
    char midiNote_speed_full_range    = 4;    //    ( note variations )
    
    char midiNote_speed_fullRev       = 96;   // C7 ( note start )
    char midiNote_speed_fullRev_index = 0;   // C7 ( note start )
    char midiNote_speed_fullRev_range = 4;    //    ( note variations )

    char midiCtrlChange_speed         = 20;   // Midi ctrl change number for speed
    char midiCtrlChange_wheel         = 21;   // Midi ctrl change number for Direction

    char midiCtrlChangeFilterOn       = 0;

    // CAR VALUES & VARIABLES :
    char speed                        = 0;   // maped from 0 to 127 
    char speedFlagNew                 = 0;   // set to one if new value != old value
    char wheel                        = 0;   // maped from 0 to 127
    char wheelFlagNew                 = 0;   // set to one if new value != old value

    char start                        = 0;   // maped from 0 to 127
    char maxSpeed                     = 0;   // maped from 0 to 127
    char minSpeed                     = 0;   // maped from 0 to 127
    char maxLeft                      = 0;   // maped from 0 to 127
    char maRight                      = 0;   // maped from 0 to 127
    


    enum carMoveState_e moveState = carSTOPED;
    enum carDirState_e  dirState  = carSTRAIGHT;
};

char isCarFrameAvailable(void);
struct frame_s* getNewCarFrame(void);
void CarToggleFilter(void);

#endif

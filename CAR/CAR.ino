
float forward_speed = 0.28; // %
float backward_speed = 0.28; // %




const int ledPin      = LED_BUILTIN;// the number of the LED pin
int ledState          = LOW;             // ledState used to set the LED
long ledtick          = 0;           // interval at which to blink (milliseconds)

long tickms;

float speedAttenuation            = 0.1;

long autoMsgTick        = 0;           // interval at which to blink (milliseconds)
int testTick = 0;
uint8_t battery       = 100;

int speed = 11999;
int wheel = 35000;

uint16_t motorMesure = 0;
uint16_t wheelMesure = 0;

char sendDataFlag = 0 ;

int networkAddress = 0;
int radioTimeSlotDelayUs = 0;
int ledTimeSlotDelayUs = 0;

void setup() {

  delay(2000);

  pinMode(ledPin, OUTPUT);     // set led pin as output
  Serial1.begin(115200);      // start xbee serial
 
  // Attach interrput on motor pwm on pin 2
  pinMode(A0, INPUT_PULLUP);
  attachInterrupt(A0, speed_int, CHANGE);

  // Attach interrput on wheel pwm on pin 3
  pinMode(A1, INPUT_PULLUP);
  attachInterrupt(A1, wheel_int, CHANGE);

  pinMode(10, OUTPUT);
  analogWriteFrequency(10, 50);
  analogWriteResolution(16);

  Serial.println("Start engines...");
  
  networkAddress =  getDeviceNetworkAdd();

  Serial.print("Netwrk add :...");
  Serial.println(networkAddress, DEC);
 
  radioTimeSlotDelayUs = (networkAddress-101)*7500;
  ledTimeSlotDelayUs = (networkAddress-101)*4;

  Serial.print("slot delays us :...");
  Serial.println(radioTimeSlotDelayUs, DEC);

}

int testVal1 = 0;
int testVal2 = 0;

char ledCnt = 0;

void loop() {

    float usecPerBit = 20000.0/65536.0;

    float mot = (float) motorMesure;

    mot = mot - 1500.0;
    Serial.print(mot); Serial.print("\t"); 

    //Serial.print(mot); Serial.print("\t");   

    uint16_t motorOutput;

    if( mot > 0 ) {
        motorOutput = (uint16_t)(1500.0 + mot*forward_speed);  // positive speed Attenuation
    }else{
        motorOutput = (uint16_t)(1500.0 + mot*backward_speed);  // negative speedAttenuation
    }


    Serial.print(motorOutput); Serial.print("\t"); 
    Serial.println("<--");

    analogWrite(10, (motorOutput)/(20000.0/65536.0));


    while (Serial1.available()) { // check if data avzailable from master
      
        uint8_t inChar = (char)Serial1.read(); 

        if( inChar == 0x88 || inChar == 0x89 ){

            ledCnt++;
            if( ledCnt >= 20 || inChar == 0x89){
              ledCnt = 0;
            }
            
            if( ledCnt == ledTimeSlotDelayUs){
              ledState = HIGH;
              digitalWrite(ledPin, ledState);
              ledtick = millis();  
            }

            delayMicroseconds(radioTimeSlotDelayUs); // radio time frame delay

            static int val1 = 700;

            if( val1 > 2600 )
              val1 = 700;
            else
              val1+=15;
              
            static int val2 = 700;

            if( val2 > 2600 )
              val2 = 700;
            else
              val2+=15;
              
            //motorMesure = val1;
            //wheelMesure = val2;
            Serial1.write(motorMesure>>8 & 0x00FF);
            Serial1.write(motorMesure & 0x00FF);
            
            Serial1.write(wheelMesure>>8 & 0x00FF);
            Serial1.write(wheelMesure & 0x00FF);


            testVal1 = 650;
            testVal2 = 750;

        }
    }

    //digitalWrite(12, LOW);

 
  

  

  
    // if LED is on we turn it of after 0.1 second
    if( ledState == HIGH ){ 
      if( (millis() - ledtick) > 100){
        ledState = LOW;
        digitalWrite(ledPin, ledState);
      }
    }

} // fin boucle principale



void speed_int(void)
{
    static long motorTick = 0;
    
    if( digitalRead(A0) == 1 ){
        motorTick = micros();
    }else{
        motorMesure = micros() - motorTick;
    }
    sendDataFlag = 1;
}

void wheel_int(void)
{
    static long wheelTick = 0;
    
    if( digitalRead(A1) == 1 ){
        wheelTick = micros();
    }else{
        wheelMesure = micros() - wheelTick;
    }
}





int getDeviceNetworkAdd(void){
  
  delay(1000);

  // Trying to enter AT mode
  tickms = millis();

  Serial1.print("+++"); // Entering AT Command mode
  
  uint8_t inChar = 1;
  uint8_t OK = 0;
  char ATDataRead[100];
  char ATDataReadPointer = 0;

  while( millis()-tickms < 20000 && OK < 2){ // we Expect "OK" reponse
    while(Serial1.available()){ 
      inChar = (char)Serial1.read(); // on lit un octet
      ATDataRead[ATDataReadPointer] = inChar;
      ATDataReadPointer++;
      if( inChar == 'O' || inChar == 'K' ){
        ATDataRead[OK] = inChar;
        OK++;
      }
    }
  }

  ATDataRead[OK] = 0;

  delay(200);
  
  Serial1.println("ATMY"); // read address
  ATDataReadPointer = 0;
  OK = 0;
  
  tickms = millis();
  
  while( OK<2 && millis()-tickms < 2000) { // we Expect "OK" reponse
    while(Serial1.available()) { 
      char inChar = (char)Serial1.read(); // on lit un octet
      ATDataRead[ATDataReadPointer] = inChar; // on lit un octet
      ATDataReadPointer++;
      if( inChar == 0x0D ){
        OK++;
      }
    }
  }

  ATDataRead[ATDataReadPointer-1] = 0;

  Serial1.println("ATCN"); // Exit command mode

  return atoi(ATDataRead);
}

#include <RPLidar.h>

/*
Detection area is square
+-------------------------------------------+
|                                           |
|                      ^                    |
|                      |                    |
|                      |                    |
|                      |                    |
|                      |                    |
|                      |Dmax                |
|                      |                    |
|                      |                    |
|                      |                    |
|                      |                    |
|          Dmax        |      Dmax          |
| <--------------------+------------------> |
|                                           |
+-------------------------------------------+
*/

#define Dmax    5000.0 // define detection square zone Area


#define Ymax          5000
#define xLeftMax     1700
#define xRightMax    1700


#define Dmin    0.20

// return 0 if not in detection area
// return 1 if in distance area
int maxDistanceAtAngle( float angle);

const int ledPin      = LED_BUILTIN;// the number of the LED pin
int ledState          = LOW;             // ledState used to set the LED
long ledtick          = 0;           // interval at which to blink (milliseconds)

int   distance;
int   angle;

const int redPin =  12;
const int greenPin =  15;
const int bluePin =  14;

long tickled;

int getDeviceNetworkAdd(void);

RPLidar lidar;

int networkAddress = 0;
int radioTimeSlotDelayUs = 0;
int ledTimeSlotDelayUs = 0;

void setup()   {     

  delay(1000);
  Serial1.begin(115200);      // start xbee serial      
  Serial.println("Scaning started...");
  
  networkAddress =  getDeviceNetworkAdd();
  Serial.print("Netwrk add :...");
  Serial.println(networkAddress, DEC);

  radioTimeSlotDelayUs = (networkAddress-101)*7500;
  ledTimeSlotDelayUs = (networkAddress-101)*4;
  
  delay(2000);
  
  for( int i=0; i<=180; i=i+10){
    Serial.print ("a:");
    Serial.print ( i, DEC); 
    Serial.print (" dmax:" );
    Serial.println ( maxDistanceAtAngle( i ));
  }

  for( int i=0; i<=360; i=i+20){
    Serial.print ("a:");
    Serial.print ( i, DEC); 
    Serial.print (" rmap " );
    Serial.println ( angleRemap( i ));
  }

  

  pinMode(ledPin, OUTPUT);        
  //Serial2.begin(115200);

  lidar.begin(Serial2);
}

float minDistance = 100000;
float angleAtMinDist = 0;

float ledPeriod = 1000;

int isAngleValid( int angle ){
  if( angle >= 270+15 && angle <= 360){
    return 1;
  }else if( angle >= 0 && angle <= 90-15){
    return 1;
  }else{
    return 0;    
  }
}

int angleRemap( int angle ){
  if( angle >= 270 && angle <= 360){
    return angle - 270;
  }else{
    return angle + 90;
  }
}

int mdistance, mangle;

int newDistance, newAngle;

int dx, dy;
int mdx, mdy;

void loop(){             

  // READ LIDAR DATA
  if (IS_OK(lidar.waitPoint())) {
    
    // get data from lidar
    distance =  lidar.getCurrentPoint().distance;
    angle    =  lidar.getCurrentPoint().angle;
    bool  startBit =  lidar.getCurrentPoint().startBit; //whether this point is belong to a new scan
    byte  quality  =  lidar.getCurrentPoint().quality; //quality of the current measurement
    bool  newScan  =  lidar.getCurrentPoint().startBit;

    if( newScan ){

             static int newDist = 0, newAng = 0;
             if( mdistance != 10000000){
              
                  Serial.print(mdistance);
                  Serial.print(", a");Serial.print(mangle);
                  Serial.print(", x");Serial.print(mdx);
                  Serial.print(", y");Serial.println(mdy);
                  
                  Serial1.write(0xAA); 
                  Serial1.write(mdistance >> 8); 
                  Serial1.write(mdistance); 
                  Serial1.write(mangle >> 8); 
                  Serial1.write(mangle); 
             }

             static char test = 0;
             
             mdistance = 10000000;
             mangle = 10000000; 
      
    }else{

        if( distance > 0 ){ //&& quality == 15) {

            angle = angleRemap( angle);

            if ( angle >= 0 && angle <= 180 ){

                  //maxDistance =  abs(Dmax / cos ( angle/57.2957795 ) );
                 
                  dx =  distance * cos(angle/57.2957795);
                  dy =  distance * sin(angle/57.2957795);

                  //Serial.print(distance);
                  //Serial.print(", a");Serial.print(angle);
                  //Serial.print(", x");Serial.print(dx);
                  //Serial.print(", y");Serial.println(dy);
                  
                  if(  ((dx > -xLeftMax) && (dx < xRightMax)) &&  (dy < Ymax) ) {
                  
                      if(  distance < mdistance ){
                         mdistance = distance;
                         mangle = angle;
                         mdx = dx;
                         mdy = dy;
                      }
                      
                  }
                 // } end if dist range
            } // end if in angle range
        }
    }

    // if LED is on we turn it of after 0.1 second
    if( ledState == HIGH ){ 
      if( (millis() - ledtick) > 100){
        ledState = LOW;
        digitalWrite(ledPin, ledState);
      }
    }



    if (lidar.getCurrentPoint().startBit) {
       // a new scan, display the previous data...
       minDistance = 100000;
       angleAtMinDist = 0;
    } else {
          minDistance = distance;
          angleAtMinDist = angle;
    }
  } else {

    // try to detect RPLIDAR... 
    rplidar_response_device_info_t info;
    if (IS_OK(lidar.getDeviceInfo(info, 100))) {
       //detected...
       lidar.startScan();
       Serial.println("Lidar start scanning...");
       delay(1000);
    }
  }

  //ledPeriod = (((float)minDistance - 150.0)/950.0) * (1000.0-40.0) + 40;
  /*if ( millis() - tickled > ledPeriod ){
      tickled = millis();
      ledState =! ledState;
      digitalWrite(ledPin, ledState);
  }*/

  

}








int getDeviceNetworkAdd(void){
  long tickms;
  delay(1000);

  // Trying to enter AT mode
  tickms = millis();

  Serial1.print("+++"); // Entering AT Command mode

  Serial.println("Entering AT Mode"); 
  
  uint8_t inChar = 1;
  uint8_t OK = 0;
  char ATDataRead[100];
  char ATDataReadPointer = 0;

  while( millis()-tickms < 20000 && OK < 2){ // we Expect "OK" reponse
    while(Serial1.available()){ 
      inChar = (char)Serial1.read(); // on lit un octet
      ATDataRead[ATDataReadPointer] = inChar;
      ATDataReadPointer++;
      if( inChar == 'O' || inChar == 'K' ){
        ATDataRead[OK] = inChar;
        OK++;
        Serial.println("AT Mode");
      }
    }
  }

  if( OK == 0 )
    Serial.println("Fail entering AT Mode"); 

  ATDataRead[OK] = 0;

  delay(200);
  
  Serial1.println("ATMY"); // read address
  ATDataReadPointer = 0;
  OK = 0;
  
  tickms = millis();
  
  while( OK<2 && millis()-tickms < 2000) { // we Expect "OK" reponse
    while(Serial1.available()) { 
      char inChar = (char)Serial1.read(); // on lit un octet
      ATDataRead[ATDataReadPointer] = inChar; // on lit un octet
      ATDataReadPointer++;
      if( inChar == 0x0D ){
        OK++;
        Serial.println("Read add ok");
      }
    }
  }

  ATDataRead[ATDataReadPointer-1] = 0;

  Serial1.println("ATCN"); // Exit command mode

  return atoi(ATDataRead);
}



// return 0 if not in detection area
// return 1 if in distance area
int maxDistanceAtAngle( float angle){
  
  float maxDistance;

  if ( angle > 90 ){
    angle = angle - 90;
  }

  if ( angle <= 45 ){
    maxDistance =  abs(Dmax / cos ( angle/57.2957795 ) );
  }else if ( angle <= 90 ){
    maxDistance =  abs(Dmax / sin ( angle/57.2957795 ) );
  }
  
  return maxDistance;
}

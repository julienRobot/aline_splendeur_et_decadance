#define Ymax          5000
#define xLeftMax     1700
#define xRightMax    1700

// constants won't change. Used here to set a pin number:
const int ledPin =  LED_BUILTIN;// the number of the LED pin
int ledState = LOW;             // ledState used to set the LED
long ledtick=0;

#define PRINTRAWDATA

#define COMPUTERSerial Serial
#define XBEESerial Serial1

struct frame_s{
  char header;
  int  lenght;
  char API;
  long SrcAdd;
  char rssi;
  char option;
  char data[256];
  char checksum;
};

struct frame_s frame;

char startup = 1;
char assignMode = 0;

void setup() {

  pinMode(ledPin, OUTPUT);                // set the led pin as output:
  
  pinMode(12, OUTPUT);                    // set test pin 12 as output
  
  pinMode(10, OUTPUT);                    // set test pin 10 as output

  pinMode(6, OUTPUT);                    // set test pin 10 as output
  
  digitalWrite(15, LOW);
  
  pinMode(8, INPUT_PULLUP);                    // set test pin 10 as output

  delay(1000);
  
  Serial1.begin(115200);                  // Xbee serial at 115200 

  delay(1000);

  Serial.println("race started..."); 

  delay(1000);

  static char FLAGZONE = 0;

  struct frame_s *newFrame;

  static char pointer = 0;

  static int distance;
  static int angle;

  static char flag = 0;

  static long lastMsgTick = 0;

  while(1){

       if( (millis() - lastMsgTick )  > 5000 && flag==1){
        
              usbMIDI.sendControlChange(50,63, 10);
             usbMIDI.sendControlChange(51,127, 10);
             Serial.println("off");
             flag = 0;
      }
    

       while ( Serial1.available() ) { // check if data from ZIG

          uint8_t inChar = (char)Serial1.read(); // read Zigbee serial port char

          //Serial.println(inChar, HEX);
          
          switch(pointer){
            
            case 0:
              if( inChar == 0xaa){
                distance = 0;
                angle = 0;
                pointer = 1;
              }else{
                pointer = 0;
              }
              
            break;

            case 1: 
              distance = inChar << 8 ;
              pointer = 2;
            break;
            
            case 2: 
              distance = distance | inChar;
              pointer = 3;
            break;
            
            case 3: 
              angle = inChar << 8 ;
              pointer = 4;
            break;
            
            case 4: 
              angle = angle | inChar;
              pointer = 0;
              
              Serial.print("d:");
              Serial.print(distance, DEC);
              Serial.print("mm, a:");
              Serial.print(angle, DEC);
              Serial.print("°\t\t\t");

              // convert to polar coordinate
              int x, y;

              x = (distance) * cos ( angle/57.2957795);
              y = (distance) * sin ( angle/57.2957795);

              Serial.print("x:");
              Serial.print(x, DEC);
              Serial.print("mm, y:");
              Serial.print(y, DEC);
              Serial.print("mm\t\t\t");

              // convert to 127 value
              float maxDist = 5000;
              
              int ccX, ccY;

              ccX = map(x, -xLeftMax,   xRightMax,  0 , 127);
              ccY = map(y,     0,   Ymax,  0, 127);

              if(ccY<0)
                ccY = 0;

              if(ccX<0)
                ccX = 0;

              if(ccX>127)
                ccX =127;

              if(ccY>127)
                ccY = 127;
              
              Serial.print("ccx:");
              Serial.print(ccX, DEC);
              Serial.print(", ccy:");
              Serial.println(ccY, DEC);

              usbMIDI.sendControlChange(50,ccX, 10);
              usbMIDI.sendControlChange(51,ccY, 10);

              //if( (millis() - lastMsgTick ) >= 3000 ){
              //    usbMIDI.sendNoteOn(12,127,  8);
              //    flag = 1;
             // }
//
              lastMsgTick = millis(); 

              flag = 1;
              
              break;
            
          }



      }


  }


      /* 
       *  check for policer to push the fucking button
       *  
       */
      long btnTick;
      static char btnInibit = 0;
      
      if( digitalRead(8) == LOW && btnInibit == 0) {
        
        btnTick = millis();
        
        while( (digitalRead(8) == LOW) && ((millis()-btnTick)<3000)  ){
        }

        if( digitalRead(8) == LOW ) {
            btnInibit = 1;
            
        }
        
      }

      if( digitalRead(8) == HIGH){
            btnInibit = 0;
      }  




  
  
  delay(1000);  

}

int rx_cnt = 0;
int rx_buff[2000] ;
int rx_pntr = 0;

long ms=0;
 // boulce principal du programme

uint8_t reverse = 0;
uint8_t forward = 0;
uint8_t left = 0;
uint8_t right = 0;
uint8_t dir = 0;

uint8_t reverse_last = 0;
uint8_t forward_last = 0;
uint8_t left_last = 0;
uint8_t right_last = 0;

char moving = 0;
char arriere = 0;
char stoping = 0;
char fw_max = 0;
char bw_max = 0;
char left_max = 0;
char right_max = 0;
char left_cnt = 0;
char right_cnt = 0; 

int radioSyncCounts = 0 ;
char radioSyncData = 0;
 
void loop() {

   // Loop back data from USB Serial to Xbee Serial
   while (Serial.available()){ 
     char dat = (char)Serial.read();
     Serial1.write(dat);
   }

  sendSyncFrame_polling(); // send sync frame every 50 ms
  
  // ZIGBEE Serial port read
  while (Serial1.available()) { // check if data from ZIG

    uint8_t inChar = (char)Serial1.read(); // read Zigbee serial port char
    
    //Serial.write(inChar); // loop back char to computer Serial USB
    
    ledState = HIGH;
    digitalWrite(ledPin, ledState);
    ledtick = millis();
   
  
  digitalWrite(10, LOW);
  }


  // if LED is on we turn it of after 0.1 second
  if( ledState == HIGH ){ 
    if( (millis() - ledtick) > 100){
      ledState = LOW;
      digitalWrite(ledPin, ledState);
    }
  }
  
} 


char buildFrame(uint8_t nuChar){

  static int pointer = 0;
  static int charCounter = 0;

  switch( pointer ){
    
      case 0: // header
        if(nuChar == 0x7E){
          pointer++;
          charCounter = 0;
          frame.lenght = 0;
          frame.SrcAdd = 0;
          frame.header = 0x7E;
          return 1;
        }else{
          pointer = 0;
          return 1;
        }
      break;
  
      case 1: // size MSB 
        frame.lenght = frame.lenght | nuChar << 8;
        pointer++;
        return 1;
      break;
  
      case 2: // size LSB
        frame.lenght = frame.lenght |  nuChar;
        pointer++;
        
        return 1;
      break;

      case 3: // size MSB 
        frame.API = nuChar;

        if( frame.API== 0x81){ // only accept message from devices
          pointer++;
        }else{
          pointer = 0;
        }
          
        return 1;
      break;

      case 4: // size MSB 
        frame.SrcAdd = frame.SrcAdd | nuChar << 8;
        pointer++;
        return 1;
      break;
  
      case 5: // size LSB
        frame.SrcAdd = frame.SrcAdd |  nuChar;
        pointer++;
        return 1;
      break;

      case 6: // size LSB
        frame.rssi = nuChar;
        pointer++;
        return 1;
      break;

     case 7: // size LSB
        frame.option = nuChar;
        pointer++;
        return 1;
      break;

      default:
        if( charCounter < (frame.lenght - 5) ){
           frame.data[charCounter] = nuChar;
           charCounter++;
           return 1;
        }else{
           frame.checksum = nuChar;
           pointer = 0;
           return 0;
        }
      break;
  }
  
}



void sendSyncFrame_polling(void){
  
     if( (millis() - ms) > 50){

      radioSyncCounts++;
      if( radioSyncCounts >= 20){
        radioSyncCounts = 0;
        radioSyncData = 0x89;
      }else{
        radioSyncData = 0x88;  
      }

      digitalWrite(12, HIGH);
    
      ms = millis();
      // From XCTU Frame generator 7E 00 06 01 01 FF FF 01 88 76
      Serial1.write(0x7E);  // start delimiter
      Serial1.write(0x00);  // lenght
      Serial1.write(0x06);  // lenght
      Serial1.write(0x01);  // frame type  ??
      Serial1.write(0x01);  // frame ID ???
      Serial1.write(0xFF);  // Add ff broadcast
      Serial1.write(0xFF);  // add ff broadcast
      Serial1.write(0x01);  // option 
      Serial1.write(0x88);  // data 
      Serial1.write(0x76);  // CRC

      digitalWrite(12, LOW);
   }
   
}





  
 

  

      


    
  

  
  


// API FRAME FORMAT
// Example :

// Start     Lenght       Type  source add                src nwrk add  Option                            Checksum
// 7E        00   14      90    00 13 A2 00 41 7B 6A BB   83 CE         01      62 6F 6E 6A 6F 75 72 0D   7B 
//           MSB  LSB     Rx Packet                                     ack     b  o  n  j  o  u  r  \r 

// source: https://www.digi.com/resources/documentation/Digidocs/90002002/Reference/r_frame_0x90.htm?TocPath=API%20Operation%7CFrame%20descriptions%7C_____10




// Tableau d'assignement des messages recu des voitures
 
// -----------------------------------------------------
// PARAMETRE                                    MSG TYPE
// -----------------------------------------------------
// volant, course gauche :                -->   CC    75
// volant, course droite :                -->   CC    75
// volant, fin de course gauche :         -->   CC    75
// volant, fin de course droite :         -->   CC    75

// gachette, sortie de l'etat de repos    -->   CC    75
// gachette, entree dans l'etat de repos  -->   CC    75
// gachette, course marche avant          -->   CC    75
// gachette, course marche arriere        -->   CC    75

  /*Serial.println("> Send +++"); // consol
  Serial1.print("+++"); // To Xbee
  switch( Xbee_isOk() ){
    case 0:   Serial.println("Entered AT Mode"); // consol
    break;
    case 1:   Serial.println("timeout..."); // consol
    break;
    case 2:   Serial.println("unknown resp..."); // consol
    break;
  }
  Serial.println("> send AT"); // consol
  Serial1.print("AT\r"); // To Xbee
  switch( Xbee_isOk() ){
    case 0:   Serial.println("Xbee detected"); // consol
    break;
    case 1:   Serial.println("timeout..."); // consol
    break;
    case 2:   Serial.println("unknown resp..."); // consol
    break;
 }
  while(1);*/

uint8_t Xbee_isOk(void){
    char Chars[2];
    char Add=0;
    long timeoutTick=0;
    timeoutTick = millis();
    while ( 1 ){ //(Add < 2)){
      if( Serial1.available() ) {
        Chars[Add]=(char)Serial1.read();
        Serial.print(Chars[Add]);
        //Add++;
      }
      if(millis()-timeoutTick > 1000 ){
          return 1;
      }
    }
    if( Chars[0] == 'O' && Chars[1] == 'K'){
      return 0;
    }else{
      return 2;
    }
    Serial.println("<<raw");
}
